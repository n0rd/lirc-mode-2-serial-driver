#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "lib/lirc_driver.h"

extern const struct driver* hardwares[];

int main()
{
	const struct driver *d = hardwares[0];
	drv.device = "/dev/ttyACM0";
	d->init_func();

	while (true)
	{
		lirc_t result = d->readdata(10000000);
		if (result != 0)
		{
			printf("%08X\n", result);
		}
	}
	
	d->deinit_func();
	
	printf("Done\n");
	return 0;
}

