#include <stdio.h>
#include <termios.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdbool.h>
#include <time.h>

#include "lib/lirc_driver.h"
#include "lib/lirc_log.h"

static int rate_to_constant(int baudrate) {
#define B(x) case x: return B##x
	switch(baudrate) {
		B(50);     B(75);     B(110);    B(134);    B(150);
		B(200);    B(300);    B(600);    B(1200);   B(1800);
		B(2400);   B(4800);   B(9600);   B(19200);  B(38400);
		B(57600);  B(115200); B(230400); B(460800); B(500000); 
		B(576000); B(921600); B(1000000);B(1152000);B(1500000); 
	default: return 0;
	}
#undef B
}    

//#define logperror(a,args...) fprintf(stderr, args)
//#define logprintf(a,args...) printf(args)

static int serial_configure(int fd)
{
	struct termios cfg;
	
	if ( !isatty(fd) )
	{
		logperror(LIRC_ERROR, "Descriptor is not a TTY device");
		return -1;
	}
	
	if ( tcgetattr(fd, &cfg) < 0 )
	{
		logperror(LIRC_ERROR, "Failed to get configuration");
		return -2;
	}
	
	cfg.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | IXON);
	cfg.c_oflag = 0;
	cfg.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
	cfg.c_cflag &= ~(CSIZE | PARENB | CSTOPB);
	cfg.c_cflag |= CS8;
	
	cfg.c_cc[VMIN] = 1;
	cfg.c_cc[VTIME] = 0;
	
	if ( cfsetispeed(&cfg, B115200) < 0 || cfsetospeed(&cfg, B115200) )
	{
		logperror(LIRC_ERROR, "Failed to set speed");
		return -3;
	}
	
	if ( tcsetattr(fd, TCSAFLUSH, &cfg) < 0 )
	{
		logperror(LIRC_ERROR, "Failed to update configuration");
		return -4;
	}
	
	return 0;
}

static int serial_open(const char *device)
{
	return open(device, O_RDWR | O_NOCTTY | O_NDELAY);
}

static int fd = -1;
static uint32_t data;
static int bytesRead;
static struct driver mode2_serial;
static uint8_t prevMSB = 42;

static lirc_t serial_readdata(lirc_t timeout);

static long getMicroseconds(struct timespec *start, struct timespec *end)
{
	return (end->tv_sec - start->tv_sec) * 1000 + (end->tv_nsec - start->tv_nsec) / 1000;
}

static int serial_init()
{
	logprintf(LIRC_DEBUG, "serial_init(), device: %s\n", drv.device == NULL ? "null" : drv.device);
	fd = serial_open(drv.device);
	logprintf(LIRC_DEBUG, "opened COM port: %d\n", fd);
	bytesRead = 0;
	data = 0;
	
	if ( fd != -1 )
	{
		int cfgResult = serial_configure(fd);
		logprintf(LIRC_DEBUG, "Configuration result: %d\n", cfgResult);
		
		// read data if any
		struct timespec start, cur;
		clock_gettime(CLOCK_MONOTONIC, &start);
		long timediff = 0;
		do
		{
			serial_readdata(1);
			clock_gettime(CLOCK_MONOTONIC, &cur);
			timediff = getMicroseconds(&start, &cur);
		} while ( timediff < 100000 );
	}
	
	drv.fd = fd;
	
	return fd != -1;
}

static int serial_deinit()
{
	logprintf(LIRC_DEBUG, "serial_deinit()\n");
	close(fd);
	return 1;
}

static char *serial_rec(struct ir_remote *remotes)
{
	logprintf(LIRC_DEBUG, "serial_rec()\n");
	if (!rec_buffer_clear())
		return (NULL);
	return (decode_all(remotes));
}

static useconds_t min(useconds_t a, useconds_t b)
{
	return a < b ? a : b;
}

static lirc_t serial_readdata(lirc_t timeout)
{
	uint32_t buffer;
	lirc_t ret = 0;
	bool mustRead = true;
	struct timespec start;
	lirc_t accumulatedTimeout = 0;
	
	clock_gettime(CLOCK_MONOTONIC, &start);
	
//	logprintf(LIRC_DEBUG, "serial_readdata(%d) %d\n", timeout, start);
	
	do
	{
		int result = read(fd, &buffer, 4);
		if ( result == -1 ) //no data
		{
			if ( timeout > accumulatedTimeout )
			{
				struct timespec curTime;
				useconds_t sleepLen = min(timeout / 2, 3000);
				usleep(sleepLen);
				clock_gettime(CLOCK_MONOTONIC, &curTime);
				accumulatedTimeout = getMicroseconds(&start, &curTime);
			}
			else if ( timeout != 0 )
			{
				// terminate if timeout hit
				mustRead = false;
			}
		}
		else
		{
//			logprintf(LIRC_DEBUG, "r=%d ", result);
			while ( result > 0 )
			{
				++bytesRead;
				--result;
				data = ((data >> 8) & 0xFFFFFF) | (buffer << 24);
				buffer >>= 8;
				if ( bytesRead >= 4 )
				{
					uint8_t msb = data >> 24;
					if ( (msb == 0 || msb == 1) && msb != prevMSB )
					{
						prevMSB = msb;
						bytesRead = 0;
						ret = data;
//						logprintf(LIRC_DEBUG, "r: %08X %d ", ret, time(NULL) - start);
						mustRead = false;
					}
					else
					{
						logprintf(LIRC_DEBUG, "Not synchronized, adjusting");
						bytesRead = 1; // shift by 1
						prevMSB = 42;  // something that is neither 0, nor 1
					}
				}
			}
		}
	} while (mustRead);
	
//	logprintf(LIRC_DEBUG, "\n");
	
	return ret;
}

static struct driver mode2_serial = 
{
	.name           = "mode2_serial",
	.device         = "",
	.features       = LIRC_CAN_REC_MODE2,
	.send_mode      = LIRC_MODE_PULSE,
	.rec_mode       = LIRC_MODE_MODE2,
	.code_length    = 0,
	.init_func      = serial_init,
	.deinit_func    = serial_deinit,
	.open_func      = default_open,
	.close_func     = default_close,
	.send_func      = NULL,
	.rec_func       = serial_rec,
	.decode_func    = receive_decode,
	.drvctl_func    = NULL,
	.readdata       = serial_readdata,
	.api_version    = 2,
	.driver_version = "0.0.1",
	.info           = "No info available"
};

const struct driver* hardwares[] = { &mode2_serial, NULL };
