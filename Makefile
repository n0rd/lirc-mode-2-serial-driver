OBJ=obj
BIN=bin
LIRC_PATH=/home/andrei/lirc-git
LIRC_LIB=lib/.libs

$(BIN)/test: $(OBJ)/test.o $(OBJ)/serial.o $(BIN)/liblirc_driver.so.0 $(BIN)/liblirc.so.0
	gcc -o $@ -L$(LIRC_PATH)/$(LIRC_LIB) -llirc_driver -llirc $(filter %.o,$^)

$(OBJ)/%.o: %.c $(OBJ)
	gcc -c -o $@ -I$(LIRC_PATH) -fPIC $(DEF) $<
	
$(OBJ):
	mkdir $(OBJ)

$(BIN):
	mkdir $(BIN)
	
$(BIN)/%.so.0: $(LIRC_PATH)/$(LIRC_LIB)/%.so.0.0.0 $(BIN)
	cp $< $@

default: $(BIN)/test

clean:
	rm -rf ./$(OBJ)
	rm -rf ./$(BIN)
	
$(BIN)/mode2_serial.so: $(OBJ)/serial.o $(BIN)/liblirc_driver.so.0 $(BIN)/liblirc.so.0
	gcc -o $@ -shared -L$(LIRC_PATH)/$(LIRC_LIB) -llirc_driver -llirc $(filter %.o,$^)

lib: $(BIN)/mode2_serial.so

install: $(BIN)/mode2_serial.so
	cp $< /home/andrei/lirc/usr/local/lib/lirc/plugins
